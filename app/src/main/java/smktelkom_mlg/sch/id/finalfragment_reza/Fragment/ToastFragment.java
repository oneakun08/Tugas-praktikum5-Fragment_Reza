package smktelkom_mlg.sch.id.finalfragment_reza.Fragment;

/**
 * Created by Root on 3/3/2018.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import smktelkom_mlg.sch.id.finalfragment_reza.R;

public class ToastFragment extends Fragment implements View.OnClickListener {
    Button pesanToast, keluar, tampilList;

    public ToastFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_multiple_toast, container, false);
        pesanToast = view.findViewById(R.id.toastBtn);
        keluar = view.findViewById(R.id.exitBtn);
        tampilList = view.findViewById(R.id.listDialogBtn);
        pesanToast.setOnClickListener(this);
        keluar.setOnClickListener(this);
        tampilList.setOnClickListener(this);

        return view;
    }

    //jangan diketik langsung di implement methode
    @Override
    public void onClick(View clicked) {
        switch (clicked.getId()) {
            case R.id.listDialogBtn:
                munculListDialog();
                break;
            case R.id.toastBtn:
                Toast.makeText(clicked.getContext(), "Kamu memilih toast", Toast.LENGTH_SHORT).show();
                break;
            case R.id.exitBtn:
                exit();
                break;
        }
    }

    //methode ini hasil dari create metod pada Exit
    private void exit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getView().getContext());
        builder.setMessage("Apakah Anda Benar ingin keluar?").setCancelable(false)
                .setPositiveButton("Ya", new
                        DialogInterface.OnClickListener() {
                            //methode ini hasil dari create metod pada OnClickListener
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                getActivity().finish();
                            }
                        }).setNegativeButton("Tidak", new
                DialogInterface.OnClickListener() {
                    //methode ini hasil dari create metod pada OnClickListener
                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.cancel();
                    }
                }).show();
    }

    //methode ini hasil dari create metod pada munculListDialog
    private void munculListDialog() {
        final CharSequence[] items = {"Programming", "Teknisi", "Designer",
                "Animator", "Developer Web"
        };
        AlertDialog.Builder kk = new AlertDialog.Builder(getView().getContext());
        kk.setTitle("Pilih Profesi");
        kk.setItems(items, new DialogInterface.OnClickListener() {
            //methode ini hasil dari create metod pada OnClickListener
            @Override
            public void onClick(DialogInterface dialog, int item) {
                Toast.makeText(getView().getContext(), items[item],
                        Toast.LENGTH_SHORT).show();
            }
        }).show();
    }
}