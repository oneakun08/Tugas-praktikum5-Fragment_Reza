package smktelkom_mlg.sch.id.finalfragment_reza.Fragment;

/**
 * Created by Root on 3/3/2018.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import smktelkom_mlg.sch.id.finalfragment_reza.R;

public class PictureFragment extends Fragment {
    public PictureFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =
                inflater.inflate(R.layout.activity_multiple_picture, container, false);
        return view;
    }
}