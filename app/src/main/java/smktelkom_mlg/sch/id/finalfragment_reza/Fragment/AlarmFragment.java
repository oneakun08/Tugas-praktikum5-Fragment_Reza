package smktelkom_mlg.sch.id.finalfragment_reza.Fragment;

/**
 * Created by Root on 3/3/2018.
 */

import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import smktelkom_mlg.sch.id.finalfragment_reza.R;


public class AlarmFragment extends Fragment {
    private ImageButton buttonAlarm;

    public AlarmFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =
                inflater.inflate(R.layout.activity_multiple_alarm, container, false);
        buttonAlarm = view.findViewById(R.id.btnjam);
        buttonAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAlarm("Waktu Pulang", 16, 30);
            }

            private void createAlarm(String message, int hour, int minutes) {
                Intent alarmIntent = new Intent(AlarmClock.ACTION_SET_ALARM);
                alarmIntent.putExtra(AlarmClock.EXTRA_MESSAGE, message);
                alarmIntent.putExtra(AlarmClock.EXTRA_HOUR, hour);
                alarmIntent.putExtra(AlarmClock.EXTRA_MINUTES, minutes);
                if (alarmIntent.resolveActivity(getActivity().getPackageManager()) !=
                        null) startActivity(alarmIntent);
            }
        });

        return view;

    }
}
