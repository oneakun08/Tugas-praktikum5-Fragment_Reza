package smktelkom_mlg.sch.id.finalfragment_reza.Fragment;

/**
 * Created by Root on 3/3/2018.
 */

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import smktelkom_mlg.sch.id.finalfragment_reza.R;

public class MapsFragment extends Fragment {
    private ImageButton buttonmaps;
    private TextView Java;

    public MapsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_multiple_maps, container, false);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        Java = view.findViewById(R.id.java_text);
        buttonmaps = view.findViewById(R.id.buttonmaps);
        buttonmaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri geoLocation = Uri.parse("geo:0,0?q=-6.870563, 107.594126(SMK Telkom Malang)?z=23");
                showMap(geoLocation);
            }

            //method showMap
            private void showMap(Uri geoLocation) {
                Intent mapIntent = new Intent(Intent.ACTION_VIEW);
                mapIntent.setData(geoLocation);
                if (mapIntent.resolveActivity(getActivity().getPackageManager())
                        != null) startActivity(mapIntent);
            }
        });
//Ketika TextView Java diklik
        Java.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.udacity.com/learn/java"));
                startActivity(intent);
            }
        });
        return view;
    }
}