package smktelkom_mlg.sch.id.finalfragment_reza.Fragment;

/**
 * Created by Root on 3/3/2018.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import smktelkom_mlg.sch.id.finalfragment_reza.R;

public class MenuFragment extends Fragment {
    Button btntoast, btnalarm, btnmaps, btnpicture;
    Fragment frag;
    FragmentTransaction fragTransaction;

    public MenuFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_fragment, container, false);
        frag = new ToastFragment();
        fragTransaction = getFragmentManager().beginTransaction().add(R.id.container, frag);
        fragTransaction.commit();
        btntoast = view.findViewById(R.id.btntoast);
        btnalarm = view.findViewById(R.id.btnalarm);
        btnmaps = view.findViewById(R.id.btnmaps);
        btnpicture = view.findViewById(R.id.btnpicture);

        btntoast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag = new ToastFragment();
                fragTransaction =
                        getFragmentManager().beginTransaction().replace(R.id.container, frag);
                fragTransaction.commit();
            }
        });
        btnalarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag = new AlarmFragment();
                fragTransaction =
                        getFragmentManager().beginTransaction().replace(R.id.container, frag);
                fragTransaction.commit();
            }
        });
        btnmaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag = new MapsFragment();
                fragTransaction =
                        getFragmentManager().beginTransaction().replace(R.id.container, frag);
                fragTransaction.commit();
            }
        });
        btnpicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag = new PictureFragment();
                fragTransaction =
                        getFragmentManager().beginTransaction().replace(R.id.container, frag);
                fragTransaction.commit();
            }
        });
        return view;
    }
}